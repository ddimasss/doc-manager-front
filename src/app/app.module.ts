
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import { FullComponent } from './layouts/full/full.component';
import { AppBlankComponent } from './layouts/blank/blank.component';


import { VerticalAppHeaderComponent } from './layouts/full/vertical-header/vertical-header.component';
import { VerticalAppSidebarComponent } from './layouts/full/vertical-sidebar/vertical-sidebar.component';
import { HorizontalAppHeaderComponent } from './layouts/full/horizontal-header/horizontal-header.component';
import { HorizontalAppSidebarComponent } from './layouts/full/horizontal-sidebar/horizontal-sidebar.component';

import { AppBreadcrumbComponent } from './layouts/full/breadcrumb/breadcrumb.component';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { SharedModule } from './shared/shared.module';
import { SpinnerComponent } from './shared/spinner.component';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {CustomerComponent, CustomerDialogContent, CustomerDialogContentMany, CustomerDialogDelete} from './customer/customer.component';
import {AddCustomerComponent} from './customer/add/add.component';
import { NewCustomerComponent } from './customer/new/new.component';
import { MainCustomerComponent, FacAddressContent } from './customer/new/main/main.component';
import { Main2CustomerComponent } from './customer/new/main2/main2.component';
import { Okved2CustomerComponent, MainOkvedDialogContent, OtherOkvedDialogContent } from './customer/new/okved2/okved2.component';
import { StaffStructCustomerComponent, StaffStructDialogContent } from './customer/new/staffStructure/staffstruct.component';
import { StaffCustomerComponent, StaffDialogContent } from './customer/new/staff/staff.component';
import {MatTabsModule} from '@angular/material/tabs';
import {TemplateComponent, TemplateDialogContent} from './templates/template.component';
import {OkvedComponent, OkvedDialogContent} from './okved/okved.component';
import { SettingsComponent, SettingsDialogContent } from './settings/settings.component';
import { SendlogComponent } from './send_logs/sendlog.component';
import { PositionComponent } from './positions/position.component';
import { UserActionsComponent } from './user_actions/useractions.component';
import { AddSettingsComponent } from './settings/add/add.component';
import {AddTemplateComponent} from './templates/add/add.component';
import {AddOkvedComponent} from './okved/add/add.component';
import {AddCompanyTypeComponent} from './companyType/add/add.component';
import {CompanyTypeComponent, CompanyTypeDialogContent} from './companyType/companyType.component';
import {CompanyCategoryComponent, CompanyCategoryDialogContent} from './companyCategory/companyType.component';
import {AddCompanyCategoryComponent} from './companyCategory/add/add.component';
import {InstructionComponent, InstructionDialogContent} from './instructions/instruction.component';
import {AddInstructionComponent} from './instructions/add/add.component';
import {TarifComponent, TarifDialogContent} from './tarif/tarif.component';
import {AddTarifComponent} from './tarif/add/add.component';
import {UserComponent, UserDialogContent} from './users/user.component';

import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule} from '@angular/material/dialog';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {AddUserComponent} from './users/add/add.component';
import {AddRoleComponent} from './roles/add/add.component';
import { RolesComponent, RoleTypeDialogContent } from './roles/roles.component';


export function HttpLoaderFactory(http: HttpClient): any {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true,
    wheelSpeed: 2,
    wheelPropagation: true
};


@NgModule({
    declarations: [
        AppComponent,
        FullComponent,
        VerticalAppHeaderComponent,
        SpinnerComponent,
        AppBlankComponent,
        VerticalAppSidebarComponent,
        AppBreadcrumbComponent,
        HorizontalAppHeaderComponent,
        HorizontalAppSidebarComponent,
        CustomerComponent,
        CustomerDialogContent,
        CustomerDialogContentMany,
        CustomerDialogDelete,
        AddCustomerComponent,
        NewCustomerComponent,
        MainCustomerComponent,
        Main2CustomerComponent,
        Okved2CustomerComponent,
        StaffStructCustomerComponent,
        StaffStructDialogContent,
        StaffCustomerComponent,
        StaffDialogContent,
        MainOkvedDialogContent,
        OtherOkvedDialogContent,
        FacAddressContent,
        TemplateComponent,
        OkvedComponent,
        OkvedDialogContent,
        SettingsComponent,
        SettingsDialogContent,
        TemplateDialogContent,
        AddTemplateComponent,
        AddSettingsComponent,
        AddOkvedComponent,
        AddCompanyTypeComponent,
        SendlogComponent,
        PositionComponent,
        UserActionsComponent,
        CompanyTypeComponent,
        CompanyTypeDialogContent,
        AddCompanyCategoryComponent,
        CompanyCategoryComponent,
        CompanyCategoryDialogContent,
        AddInstructionComponent,
        InstructionComponent,
        InstructionDialogContent,
        TarifComponent,
        TarifDialogContent,
        AddTarifComponent,
        UserComponent,
        UserDialogContent,
        AddUserComponent,
        AddRoleComponent,
        RoleTypeDialogContent,
        RolesComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FlexLayoutModule,
        HttpClientModule,
        MatTabsModule,
        PerfectScrollbarModule,
        SharedModule,
        NgMultiSelectDropDownModule.forRoot(),
        RouterModule.forRoot(AppRoutes, { relativeLinkResolution: 'legacy' }),
        HttpClientModule,
        TranslateModule.forRoot({
            defaultLanguage: 'en',
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        MatPaginatorModule,
        MatTableModule,
        MatFormFieldModule,
        MatCardModule,
        MatDialogModule,
        MatIconModule,
        MatMenuModule,
        MatSidenavModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatInputModule,
        FormsModule,
        MatSelectModule,
        MatButtonModule,
        MatListModule,
        MatSnackBarModule,
        MatProgressBarModule,
        MatAutocompleteModule,
        ReactiveFormsModule
    ],
    providers: [
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        },
        DatePipe
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
