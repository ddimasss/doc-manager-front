import { TestBed } from '@angular/core/testing';

import { DocbackService } from './docback.service';

describe('DocbackService', () => {
  let service: DocbackService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DocbackService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
