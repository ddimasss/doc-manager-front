import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IInstruction} from './interfaces';
import {MatSnackBar} from '@angular/material/snack-bar';
import {User} from '../users/user';

export interface CompanyType {
  id?: number;
  full_name: string;
  short_name: string;
  description?: string;
  is_deleted: boolean;
}
export interface CustomerCategory {
  id?: number;
  name: string;
  description?: string;
  is_deleted: boolean;
}
export interface Okved {
  id?: number;
  name: string;
  okved: string;
  description?: string;
  is_deleted: boolean;
}
export interface Tariff {
  id?: number;
  name: string;
  description?: string;
  is_deleted: boolean;
}
export interface Roles {
  id?: number;
  title: string;
}
// export interface Position {
//   id?: number;
//   name_i: string;
//   name_r: string;
//   name_d: string;
//   name_v: string;
//   name_t: string;
//   description?: string;
//   start_date: Date;
//   end_date?: Date;
// }

@Injectable({
  providedIn: 'root'
})
export class DocbackService {
  currentUser: User|null = null;
  redirectUrl = '';
  localData: any = {};
  localDataCopy: any = {};
  newCustomerId = 'new';
  baseUrl = 'http://78.36.197.114:54004/fin_api/';
  //baseUrl = 'http://127.0.0.1:4300/fin_api/';
  companyTypes: CompanyType[] = [];
  customerCategories: CustomerCategory[] = [];
  okveds: Okved[] = [];
  tariffs: Tariff[] = [];
  roles: Roles[] = [];
  employeeCount = [
    {key: 1, value: 'Один'},
    {key: 2, value: 'Много'}
  ];
  addressTypes = [
    'ACTUAL',
    'LEGAL',
    'MAILING'
  ];
  permissions = [
    {eng: 'customers_view', rus: 'Просмотр клиентов'},
    {eng: 'customers_edit', rus: 'Редактирование клиентов'},
    {eng: 'customer_send', rus: 'Отправка документов'},
    {eng: 'users_view', rus: 'Просмотр пользователей'},
    {eng: 'users_edit', rus: 'Редактирование пользователей'},
    {eng: 'settings_edit', rus: 'Редактирование настроек'},
    {eng: 'settings_view', rus: 'Просмотр настроек'},
  ];
  utc = [
    {id: 2, name: '+2'},
    {id: 3, name: '+3'},
    {id: 4, name: '+4'},
    {id: 5, name: '+5'},
    {id: 6, name: '+6'},
    {id: 7, name: '+7'},
    {id: 8, name: '+8'},
    {id: 9, name: '+9'},
    {id: 10, name: '+10'},
    {id: 11, name: '+11'},
    {id: 11, name: '+12'}
  ];
  messages = new Map();
  instructionsMap = new Map();
  constructor(private docBack: HttpClient, private snackBar: MatSnackBar) {
    const strUser = localStorage.getItem('currentUser');
    if (strUser) {
      this.currentUser = JSON.parse(strUser);
    }
    this.getList<CompanyType>(0, 100000, 'company_type').subscribe(
      data => {
        this.companyTypes = data;
      }
    );
    this.getList<CustomerCategory>(0, 100000, 'customer_category').subscribe(
      data => {
        this.customerCategories = data;
      }
    );
    this.getList<Okved>(0, 100000, 'okved').subscribe(
      data => {
        this.okveds = data;
      }
    );
    this.getList<Tariff>(0, 100000, 'tariff').subscribe(
      data => {
        this.tariffs = data;
      }
    );
    this.getList<IInstruction>(0, 100000, 'instruction').subscribe(
      data  => {
        data.map( instr => {
            this.instructionsMap.set(instr.id, instr);
          }
        );
      }
    );
    this.getList<Roles>(0, 100000, 'roles').subscribe(
      data => {
        this.roles = data;
      }
    );
  }
  getRoleType(RoleTypeId: number): string {
      const categoryArray = this.roles;
      for (const category of categoryArray) {
        if (category.id === RoleTypeId) {
          return(category.title);
        }
      }
      return '';
  }
  downloadFile(): void {
    const url = `${this.baseUrl}send_logs/get_xlsx`; // replace with your file URL
    this.docBack.get(url, {
      responseType: 'blob',
      headers: new HttpHeaders({
        'X-User': this.currentUser ? this.currentUser.email : ''})})
      .subscribe((blob: Blob) => {
      const downloadLink = document.createElement('a');
      downloadLink.href = URL.createObjectURL(blob);
      downloadLink.download = 'logs.xlsx'; // replace with your file name
      downloadLink.click();
    });
  }
  downloadFile2(): void {
    const url = `${this.baseUrl}send_logs/get_xlsx_ua`; // replace with your file URL
    this.docBack.get(url, {
      responseType: 'blob',
      headers: new HttpHeaders({
        'X-User': this.currentUser ? this.currentUser.email : ''})})
      .subscribe((blob: Blob) => {
        const downloadLink = document.createElement('a');
        downloadLink.href = URL.createObjectURL(blob);
        downloadLink.download = 'logs.xlsx'; // replace with your file name
        downloadLink.click();
      });
  }
  getAddressType(addressEngType: string): string {
    if (addressEngType === 'MAILING') {
      return 'Почтовый';
    }
    if (addressEngType === 'LEGAL') {
      return 'Юридический';
    }
    return 'Фактический';
  }
  getList<T>(page: number, limit: number, model: string, unique: number = 0): Observable<T[]> {
    let href = `${this.baseUrl}${model}/get_list?page=${page}&limit=${limit}`;
    if (unique > 0) {
      href += '&unique=1';
    }
    if (model.includes('/')) {
      href = `${this.baseUrl}${model}?page=${page}&limit=${limit}`;
    }
    return this.docBack.get<T[]>(href, {
      responseType: 'json',
      headers: new HttpHeaders({
        'X-User': this.currentUser ? this.currentUser.email : ''})});
  }
  // getStaffStruct(page: number, limit: number, id: number): Observable<Position[]> {
  //   const href = `${this.baseUrl}customer/get_stuff_struct/${id}?page=${page}&limit=${limit}`;
  //   return this.docBack.get<Position[]>(href);
  // }
  // getById<T>(model: string, id: number): Observable<T[]> {
  //   const href = `${this.baseUrl}${model}/get/${id}`;
  //   return this.docBack.get<T[]>(href);
  // }
  getCustomerById(id: string): Observable<any> {
    const href = `${this.baseUrl}customer/get_all/${id}`;
    return this.docBack.get<any>(href, {
      responseType: 'json',
      headers: new HttpHeaders({
        'X-User': this.currentUser ? this.currentUser.email : ''})});
  }
  openSnackBar(text: string): void {
    this.snackBar.open(text, 'Error', {
      duration: 25 * 1000,
      panelClass: ['style-error']
    });
  }
  checkPermission(url: string, action = 'no access'): boolean {
    let perms: string[] = [];
    if (!this.currentUser) {
      return false;
    }
    if (!this.currentUser.permissions) {
      return false;
    } else {
      perms = this.currentUser.permissions;
    }
    if (url === '/customers' && perms.includes('customers_view')) {
      return true;
    }
    if (url.indexOf('new_customer') !== -1 && perms.includes('customers_edit')) {
      return true;
    }
    if (url === '/users' && perms.includes('users_view')) {
      return true;
    }
    if (url === '/roles' && perms.includes('users_view')) {
      return true;
    }
    if (url === '/settings' && perms.includes('settings_view')) {
      return true;
    }
    if (url === '/user-actions' && perms.includes('settings_view')) {
      return true;
    }
    if (url.indexOf('/db/') !== -1) {
      return true;
    }
    if (url.indexOf('/logs/') !== -1 && perms.includes('settings_view')) {
      return true;
    }
    if (url === '/roles' && perms.includes('users_view')) {
      return true;
    }
    if (url === '/templates' && perms.includes('settings_view')) {
      return true;
    }
    return perms.includes(action);
  }
}
