export interface Message {
  id: number;
  name: string;
  text: string;
  is_deleted: boolean;
}

export interface IHashMessage {
  [id: number]: Message;
}

export interface IInstruction {
  id: number;
  name: string;
  text: string;
  is_deleted: boolean;
}

export interface IHashInstruction {
  [id: number]: IInstruction;
}
