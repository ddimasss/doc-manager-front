import { Component, OnInit, Inject, Optional, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
import { AddSettingsComponent } from './add/add.component';
import { Settings } from './settings';
import { DocbackService } from '../services/docback.service';
import {TranslateService} from '@ngx-translate/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
    templateUrl: './settings.component.html'
})


export class SettingsComponent implements OnInit, AfterViewInit {
    @ViewChild(MatTable, { static: true }) table: MatTable<any> = Object.create(null);
    searchText: any;
    displayedColumns: string[] = ['#', 'setting_name', 'setting_value', 'action'];
    dataSource: MatTableDataSource<Settings> = new MatTableDataSource<Settings>([]);
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator = Object.create(null);
    settings: Settings[] = [];
    constructor(
      public dialog: MatDialog,
      public datePipe: DatePipe,
      private router: Router,
      public docBack: DocbackService,
      private http: HttpClient,
      translate: TranslateService) {
      translate.setDefaultLang('en');
      translate.use('ru');
    }

    ngOnInit(): void {
      this.docBack.getList<Settings>(0, 10000, 'settings').subscribe(data => {
        this.dataSource.data = data;
        console.log(data);
      });
    }

    ngAfterViewInit(): void {
        this.dataSource.paginator = this.paginator;
    }

    applyFilter(filterValue: string): void {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    openDialog(action: string, obj: any): void {
        obj.action = action;
        const dialogRef = this.dialog.open(SettingsDialogContent, {
            data: obj
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            if (result.event === 'Add') {
              this.addRowData();
            } else if (result.event === 'Update') {
              this.updateRowData(result.data);
            } else if (result.event === 'Delete') {
              this.deleteRowData();
            } else if (result.event === 'Error') {
              this.dialog.open(AddSettingsComponent);
            }
          }
        });
    }
    addRowData(): void {
      window.location.reload();
    }

    // tslint:disable-next-line - Disables all
    updateRowData(row_obj: Settings): boolean | any {
      const href = `${this.docBack.baseUrl}settings/edit`;
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'X-User': this.docBack.currentUser ? this.docBack.currentUser.email : '',
        })
      };
      this.http.post(href, row_obj, httpOptions).subscribe(
        () => {
          this.docBack.openSnackBar('Обновили');
        },
        error => {
          this.docBack.openSnackBar('Error' + error);
        }
      );
      window.location.reload();
        // this.dataSource.data = this.dataSource.data.filter((value: any) => {
        //     if (value.id === row_obj.id) {
        //         value.Name = row_obj.Name;
        //         value.Position = row_obj.Position;
        //         value.Email = row_obj.Email;
        //         value.Mobile = row_obj.Mobile;
        //         value.DateOfJoining = row_obj.DateOfJoining;
        //         value.Salary = row_obj.Salary;
        //         value.Projects = row_obj.Projects;
        //         value.imagePath = row_obj.imagePath;
        //     }
        //     return true;
        // });
    }

    // tslint:disable-next-line - Disables all
    deleteRowData(): boolean | any {
      window.location.reload();
    }
}


@Component({
    // tslint:disable-next-line: component-selector
    selector: 'dialog-content',
    templateUrl: 'dialog-content.html',
    styleUrls: ['./dialog.component.scss']
})
// tslint:disable-next-line: component-class-suffix
export class SettingsDialogContent {
    action: string;
    // tslint:disable-next-line - Disables all
    local_data: any;
    file: any;
    joiningDate: any = '';
    formData = new FormData();
    constructor(
        public docBack: DocbackService,
        public datePipe: DatePipe,
        public dialogRef: MatDialogRef<SettingsDialogContent>,
        // @Optional() is used to prevent error if no data is passed
        @Optional() @Inject(MAT_DIALOG_DATA) public data: Settings
    ) {
        this.local_data = { ...data };
        this.action = this.local_data.action;
        if (this.local_data.DateOfJoining !== undefined) {
            this.joiningDate = this.datePipe.transform(new Date(this.local_data.DateOfJoining), 'yyyy-MM-dd');
        }
    }
    log1(a: string): void {
      console.log(a);
    }

    doAction(): void {
      this.dialogRef.close({ event: 'Update', data: this.local_data });
    }
    closeDialog(): void {
        this.dialogRef.close({ event: 'Cancel' });
    }
    clickEd(event: any): void {
      console.log(event);
      event.target.firstChild.click();
    }

    selectFile(event: any): void {
        if (!event.target.files[0] || event.target.files[0].length === 0) {
            // this.msg = 'You must select a file';
            return;
        }
        const file: File = event.target.files[0];
        if (file) {
          this.file = file;
          this.local_data.file_path = file.name;
        }
        // const reader = new FileReader();
        // reader.readAsDataURL(event.target.files[0]);
        // // tslint:disable-next-line - Disables all
        // reader.onload = (_event) => {
        //     // tslint:disable-next-line - Disables all
        //     this.local_data.imagePath = reader.result;
        // };
    }

}
