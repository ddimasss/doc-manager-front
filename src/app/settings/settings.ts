export interface Settings {
    id?: number;
    settings_name: string;
    settings_value: string;
}

// CREATE TABLE public.templates (
// 	id serial4 NOT NULL,
// 	"name" varchar NOT NULL,
// 	employee_count int4 NOT NULL,
// 	file_path varchar NOT NULL,
// 	message_id int4 NOT NULL,
// 	doc_start_date date NOT NULL,
// 	gen_start_date date NULL,
// 	npa_date date NULL,
// 	is_deleted bool NOT NULL DEFAULT false,
// 	instruction_id int4 NOT NULL DEFAULT 1,
// 	npa_name varchar NULL
// );
