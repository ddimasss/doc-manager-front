import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Position } from './position';
import { DocbackService } from '../services/docback.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
    templateUrl: './position.component.html'
})


export class PositionComponent implements OnInit, AfterViewInit {
    @ViewChild(MatTable, { static: true }) table: MatTable<any> = Object.create(null);
    searchText: any;
    displayedColumns: string[] = ['#', 'name_i', 'name_r', 'name_d', 'name_v', 'name_t'];
    dataSource: MatTableDataSource<Position> = new MatTableDataSource<Position>([]);
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator = Object.create(null);
    settings: Position[] = [];
    constructor(
      public docBack: DocbackService,
      translate: TranslateService
    ) {
      translate.setDefaultLang('en');
      translate.use('ru');
    }

    ngOnInit(): void {
      this.docBack.getList<Position>(0, 10000, 'position', 1).subscribe(data => {
        // data.forEach((obj: any) => {
        //   obj.user_name = obj.user.first_name + ' ' + obj.user.last_name;
        //   obj.instruction_name = obj.instruction.name;
        //   obj.customer_short_name = obj.customer.short_name;
        // });
        this.dataSource.data = data;
      });
    }

    ngAfterViewInit(): void {
        this.dataSource.paginator = this.paginator;
    }

    applyFilter(filterValue: string): void {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
}
