export interface Position {
    id?: number;
    name_i: string;
    name_r: string;
    name_d: string;
    name_v: string;
    name_t: string;
    description: string;
    start_date: Date;
    end_date?: Date;
    customer_id: number;
    seo: boolean;
    sdl: boolean;
}

// CREATE TABLE public.templates (
// 	id serial4 NOT NULL,
// 	"name" varchar NOT NULL,
// 	employee_count int4 NOT NULL,
// 	file_path varchar NOT NULL,
// 	message_id int4 NOT NULL,
// 	doc_start_date date NOT NULL,
// 	gen_start_date date NULL,
// 	npa_date date NULL,
// 	is_deleted bool NOT NULL DEFAULT false,
// 	instruction_id int4 NOT NULL DEFAULT 1,
// 	npa_name varchar NULL
// );
