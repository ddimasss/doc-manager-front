import { Routes } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';
import { AppBlankComponent } from './layouts/blank/blank.component';
import { CustomerComponent } from './customer/customer.component';
import { RolesComponent } from './roles/roles.component';
import { NewCustomerComponent } from './customer/new/new.component';
import {TemplateComponent} from './templates/template.component';
import {OkvedComponent} from './okved/okved.component';
import {CompanyTypeComponent} from './companyType/companyType.component';
import { SettingsComponent } from './settings/settings.component';
import { SendlogComponent } from './send_logs/sendlog.component';
import {CompanyCategoryComponent} from './companyCategory/companyType.component';
import {InstructionComponent} from './instructions/instruction.component';
import {TarifComponent} from './tarif/tarif.component';
import {UserComponent} from './users/user.component';
import {AuthGuard} from './authentication/guard/auth.guard';
import {UserActionsComponent} from './user_actions/useractions.component';
import {PositionComponent} from './positions/position.component';

export const AppRoutes: Routes = [
    {
        path: '',
        component: FullComponent,
        children: [
          {
              path: 'customers',
              component: CustomerComponent,
              data: {
                title: 'Clients',
              },
             canActivate: [AuthGuard],
            },
          {
            path: 'new_customers',
            component: NewCustomerComponent,
            data: {
              title: 'New',
            },
            canActivate: [AuthGuard],
          },
          {
            path: 'templates',
            component: TemplateComponent,
            data: {
              title: 'Шаблоны',
            },
            canActivate: [AuthGuard],
          },
          {
            path: 'users',
            component: UserComponent,
            canActivate: [AuthGuard],
            data: {
              title: 'Пользователи',
            }
          },
          {
            path: 'settings',
            component: SettingsComponent,
            canActivate: [AuthGuard],
            data: {
              title: 'Шаблоны',
            }
          },
          {
            path: 'db',
            canActivate: [AuthGuard],
            children: [
              {
                path: 'okved',
                component: OkvedComponent,
                data: {
                  title: 'ОКВЭДы',
                }
              },
              {
                path: 'position',
                component: PositionComponent,
                data: {
                  title: 'Должности',
                }
              },
              {
                path: 'company-type',
                component: CompanyTypeComponent,
                data: {
                  title: 'ОПФ',
                }
              },
              {
                path: 'roles',
                component: RolesComponent,
                data: {
                  title: 'Roles',
                },
               canActivate: [AuthGuard],
              },
              {
                path: 'company-category',
                component: CompanyCategoryComponent,
                data: {
                  title: 'Категория клиента',
                }
              },
              {
                path: 'instruction',
                component: InstructionComponent,
                data: {
                  title: 'Типы инструктажей',
                }
              },
              {
                path: 'tarif',
                component: TarifComponent,
                data: {
                  title: 'Тарифы',
                }
              }
            ]
          },
          {
            path: 'logs',
            canActivate: [AuthGuard],
            children: [
              {
                path: 'edits',
                // canActivate: [AuthGuard],
                component: UserActionsComponent,
                data: {
                  title: 'Логи изменений',
                }
              },
              {
                path: 'events',
                // canActivate: [AuthGuard],
                component: SendlogComponent,
                data: {
                  title: 'Логи событий',
                }
              },
            ]
          },
          {
            path: 'user-actions',
            canActivate: [AuthGuard],
            component: UserActionsComponent,
            data: {
              title: 'Журнал действий пользователя',
            }
          },
            {
                path: '',
                redirectTo: '/authentication/login',
                pathMatch: 'full'
            },
        ]
    },
    {
        path: '',
        component: AppBlankComponent,
        children: [
            {
                path: 'authentication',
                loadChildren:
                    () => import('./authentication/authentication.module').then(m => m.AuthenticationModule)
            }
        ]
    },
    {
        path: '**',
        redirectTo: 'authentication/404'
    }
];
