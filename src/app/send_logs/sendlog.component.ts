import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { SendingLogs } from './SendingLogs';
import { DocbackService } from '../services/docback.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
    templateUrl: './sendlog.component.html'
})


export class SendlogComponent implements OnInit, AfterViewInit {
    @ViewChild(MatTable, { static: true }) table: MatTable<any> = Object.create(null);
    searchText: any;
    displayedColumns: string[] = ['#', 'user_id', 'created_at', 'customer_id', 'date', 'instruction_id', 'status'];
    dataSource: MatTableDataSource<SendingLogs> = new MatTableDataSource<SendingLogs>([]);
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator = Object.create(null);
    settings: SendingLogs[] = [];
    constructor(
      public docBack: DocbackService,
      translate: TranslateService
    ) {
      translate.setDefaultLang('en');
      translate.use('ru');
    }

    ngOnInit(): void {
      this.docBack.getList<SendingLogs>(0, 10000, 'send_logs').subscribe(data => {
        data.forEach((obj: any) => {
          obj.user_name = obj.user.first_name + ' ' + obj.user.last_name;
          obj.instruction_name = obj.instruction.name;
          obj.customer_short_name = obj.customer.short_name;
        });
        this.dataSource.data = data;
      });
    }

    ngAfterViewInit(): void {
        this.dataSource.paginator = this.paginator;
    }

    applyFilter(filterValue: string): void {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
}
