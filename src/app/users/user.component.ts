import { Component, OnInit, Inject, Optional, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
import { AddUserComponent } from './add/add.component';
import { User } from './user';
import { DocbackService } from '../services/docback.service';
import {TranslateService} from '@ngx-translate/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
    templateUrl: './user.component.html'
})


export class UserComponent implements OnInit, AfterViewInit {
    @ViewChild(MatTable, { static: true }) table: MatTable<any> = Object.create(null);
    searchText: any;
    displayedColumns: string[] = ['#', 'email', 'is_deleted', 'first_name', 'last_name',  'roles', 'actions'];
    dataSource: MatTableDataSource<User> = new MatTableDataSource<User>([]);
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator = Object.create(null);
    users: User[] = [];
    constructor(
      public dialog: MatDialog,
      public datePipe: DatePipe,
      private router: Router,
      public docBack: DocbackService,
      translate: TranslateService) {
        translate.setDefaultLang('en');
        translate.use('ru');
      }

      ngOnInit(): void {
      this.docBack.getList<User>(0, 10000, 'users').subscribe(data => {
        this.dataSource.data = data;
        console.log(data);
      });
    }

    ngAfterViewInit(): void {
        this.dataSource.paginator = this.paginator;
    }

    applyFilter(filterValue: string): void {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    openDialog(action: string, obj: any): void {
        obj.action = action;
        const dialogRef = this.dialog.open(UserDialogContent, {
            data: obj
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            if (result.event === 'Add') {
              this.addRowData();
            } else if (result.event === 'Update') {
              this.updateRowData();
            } else if (result.event === 'Delete') {
              this.deleteRowData();
            } else if (result.event === 'Error') {
              this.dialog.open(AddUserComponent);
            }
          }
        });
    }
    addRowData(): void {
      window.location.reload();
    }

    // tslint:disable-next-line - Disables all
    updateRowData(): boolean | any {
      window.location.reload();
        // this.dataSource.data = this.dataSource.data.filter((value: any) => {
        //     if (value.id === row_obj.id) {
        //         value.Name = row_obj.Name;
        //         value.Position = row_obj.Position;
        //         value.Email = row_obj.Email;
        //         value.Mobile = row_obj.Mobile;
        //         value.DateOfJoining = row_obj.DateOfJoining;
        //         value.Salary = row_obj.Salary;
        //         value.Projects = row_obj.Projects;
        //         value.imagePath = row_obj.imagePath;
        //     }
        //     return true;
        // });
    }

    // tslint:disable-next-line - Disables all
    deleteRowData(): boolean | any {
      window.location.reload();
    }
}


@Component({
    // tslint:disable-next-line: component-selector
    selector: 'dialog-content',
    templateUrl: 'dialog-content.html',
    styleUrls: ['./dialog.component.scss']
})
// tslint:disable-next-line: component-class-suffix
export class UserDialogContent {
    action: string;
    // tslint:disable-next-line - Disables all
    local_data: any;
    local_data_copy: any;
    file: any;
    joiningDate: any = '';
    formData = new FormData();
    constructor(
        private http: HttpClient,
        public docBack: DocbackService,
        public datePipe: DatePipe,
        public dialogRef: MatDialogRef<UserDialogContent>,
        // @Optional() is used to prevent error if no data is passed
        @Optional() @Inject(MAT_DIALOG_DATA) public data: User
    ) {
        this.local_data = { ...data };
        this.action = this.local_data.action;
        this.local_data_copy = JSON.parse(JSON.stringify(this.local_data));
        if (this.local_data.DateOfJoining !== undefined) {
            this.joiningDate = this.datePipe.transform(new Date(this.local_data.DateOfJoining), 'yyyy-MM-dd');
        }
    }
    log1(a: string): void {
      console.log(a);
    }

    doAction(): void {
      if (this.local_data.password !== this.local_data.password2) {
        alert('Пароли не совпадают');
        return;
      }
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'X-User': this.docBack.currentUser ? this.docBack.currentUser.email : '',
        })
      };
      if (this.local_data.action === 'Delete') {
        const href = `${this.docBack.baseUrl}users/edit`;
        this.local_data.is_deleted = true;
        this.local_data.copy = this.local_data_copy;
        this.http.post(href, this.local_data, httpOptions).subscribe(
          template => {
            this.dialogRef.close({event: this.action, data: template});
          },
          error => {
            this.dialogRef.close({event: 'Error', data: error});
          }
        );
      }
      if (this.local_data.action === 'Add') {
        const href = `${this.docBack.baseUrl}users/add`;
        this.http.post(href, this.local_data, httpOptions).subscribe(
          template => {
            this.dialogRef.close({event: this.action, data: template});
          },
          error => {
            this.dialogRef.close({event: 'Error', data: error});
          }
        );
      }
      if (this.local_data.action === 'Update') {
        const href = `${this.docBack.baseUrl}users/edit`;
        this.local_data.copy = this.local_data_copy;
        this.http.post(href, this.local_data, httpOptions).subscribe(
          template => {
            this.dialogRef.close({event: this.action, data: template});
          },
          error => {
            this.dialogRef.close({event: 'Error', data: error});
          }
        );
      }
    }
    closeDialog(): void {
        this.dialogRef.close({ event: 'Cancel' });
    }
    clickEd(event: any): void {
      console.log(event);
      event.target.firstChild.click();
    }
}
