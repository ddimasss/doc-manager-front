export interface User {
    id?: number;
    email: string;
    first_name: string;
    last_name: string;
    sur_name: string;
    password: string;
    description?: string;
    is_deleted: boolean;
    permissions?: string[];
}

// id: Optional[int] = None
// email: str
// first_name: str
// last_name: str
// sur_name: Optional[str]
// password: str
// description: Optional[str]
// is_deleted: bool = False
// permissions: Optional[
//   List[
//     Literal[
//       'customers_view',
//         'customers_edit',
//         'customer_send',
//         'users_veiw',
//         'users_edit',
//         'settings_edit',
//         'settings_view'
//       ]
//     ]
//   ] = []
