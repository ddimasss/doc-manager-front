import {Component, OnInit, ViewChild, Optional, Inject} from '@angular/core';
import { DocbackService } from '../../../services/docback.service';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {Customer, Address } from '../../customer';
import {MatPaginator} from '@angular/material/paginator';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {DatePipe} from '@angular/common';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-main-cust',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainCustomerComponent implements OnInit {
  @ViewChild('facTable', { static: true }) tableAddresses: MatTable<Address> = Object.create(null);
  @ViewChild('paginatorFac', { static: true }) paginatorMain: MatPaginator = Object.create(null);
  displayedColumnsFac: string[] = ['#', 'address', 'date_start', 'date_end', 'type', 'action'];
  dataSourceFac: MatTableDataSource<Address> = new MatTableDataSource<Address>([]);
  constructor(private http: HttpClient, public docBack: DocbackService, public dialog: MatDialog, private snackBar: MatSnackBar) { }
  openSnackBar(text: string): void {
    this.snackBar.open(text, 'Error', {
      duration: 25 * 1000,
      panelClass: ['style-error']
    });
  }
  checkITN(): void {
    const url = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/findById/party';
    const token = 'e06d345e05e7bfcb8154924d610c7b50f9dc1745';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-User': this.docBack.currentUser ? this.docBack.currentUser.email : '',
        Authorization: 'Token ' + token
      })
    };
    const data = {
      query: this.docBack.localData.main.itn,
    };
    this.http.post(url, data, httpOptions).subscribe(
      (customer: any) => {
        const cloudCust = customer.suggestions[0].data;
        console.log(cloudCust);
        this.docBack.localData.main.kpp = cloudCust.kpp;
        this.docBack.localData.main.ogrn = cloudCust.ogrn;
        this.docBack.localData.main.full_name = cloudCust.name.full_with_opf;
        this.docBack.localData.main.short_name = cloudCust.name.short_with_opf;
        this.docBack.localData.main.phone = cloudCust.phones;
        this.docBack.localData.main.email = cloudCust.emails;
        this.docBack.localData.main.addresses.push({
          address: cloudCust.address.value,
          date_start: new Date(cloudCust.state.registration_date),
          type: 'LEGAL'
        });
        this.tableAddresses.renderRows();
        this.dataSourceFac.paginator = this.paginatorMain;
      },
      error => {
        console.log(error);
      }
    );
  }

  openDialogMain(action: string, obj: any): void {
    obj.action = action;
    const dialogRef = this.dialog.open(FacAddressContent, {
      data: obj
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        if (result.event === 'Add') {
          this.addRowDataMain(result.data);
        } else if (result.event === 'Update') {
          this.updateRowDataMain(result.data);
        } else if (result.event === 'Delete') {
          this.deleteRowDataMain(result.data);
        } else if (result.event === 'Cancel') {
          obj.action = '';
        }
      } else {
        obj.action = '';
      }
    });
  }
  applyFilterFac(filterValue: string): void {
    this.dataSourceFac.filter = filterValue.trim().toLowerCase();
  }
  addRowDataMain(rowObj: any): void {
    const index = this.docBack.localData.main.addresses.findIndex((value: any) => {
      return value.type === rowObj.type && value.date_start === rowObj.date_start;
    });
    if (index !== -1) {
      this.openSnackBar('Этот тип адреса с этой датой уже существует!');
      return;
    }
    this.docBack.localData.main.addresses.push(rowObj);
    this.tableAddresses.renderRows();
    this.dataSourceFac.paginator = this.paginatorMain;
  }
  updateRowDataMain(rowObj: any): boolean | any {
    console.log(rowObj);
    console.log(this.docBack.localData.main.addresses);
    const updatedIndex = this.docBack.localData.main.addresses.findIndex((value: any) => {
      return value.type === rowObj.type && value.date_start === rowObj.date_start;
    });
    rowObj.action = 'Update';
    this.docBack.localData.main.addresses[updatedIndex] = rowObj;
    this.dataSourceFac.data = this.docBack.localData.main.addresses;
    this.tableAddresses.renderRows();
    this.dataSourceFac.paginator = this.paginatorMain;
  }
  deleteRowDataMain(rowObj: Customer): boolean | any {
    console.log(rowObj);
    this.tableAddresses.renderRows();
  }

  ngOnInit(): void {
    if (!('main' in this.docBack.localData)) {
      this.docBack.localData.main = {};
    }
    if (!('addresses' in this.docBack.localData.main) || (this.docBack.localData.main.addresses === null)) {
      this.docBack.localData.main.addresses = [];
    }
    this.dataSourceFac.data = this.docBack.localData.main.addresses;
    // this.tableAddresses.renderRows();
    this.dataSourceFac.paginator = this.paginatorMain;
  }

}

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'fac-address-content',
  templateUrl: 'main-okved-dialog-content.html',
})
// tslint:disable-next-line: component-class-suffix
export class FacAddressContent {
  action: string;
  // tslint:disable-next-line - Disables all
  local_data: any;
  userForm: any;

  constructor(
    public docBack: DocbackService,
    public datePipe: DatePipe,
    public dialogRef: MatDialogRef<FacAddressContent>,
    // @Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Customer) {
    this.local_data = { ...data };
    this.action = this.local_data.action;
  }
  doAction(): void {
      this.dialogRef.close({event: this.action, data: this.local_data});
  }
  closeDialog(): void {
    this.local_data.action = '';
    this.dialogRef.close({ event: 'Cancel',  data: this.local_data });
  }
  clickEd(event: any): void {
    event.target.firstChild.click();
  }

  // selectFile(event: any): void {
  //     if (!event.target.files[0] || event.target.files[0].length === 0) {
  //         // this.msg = 'You must select an image';
  //         return;
  //     }
  //     const mimeType = event.target.files[0].type;
  //     if (mimeType.match(/image\/*/) == null) {
  //         // this.msg = "Only images are supported";
  //         return;
  //     }
  //     // tslint:disable-next-line - Disables all
  //     const reader = new FileReader();
  //     reader.readAsDataURL(event.target.files[0]);
  //     // tslint:disable-next-line - Disables all
  //     reader.onload = (_event) => {
  //         // tslint:disable-next-line - Disables all
  //         this.local_data.imagePath = reader.result;
  //     };
  // }

}
