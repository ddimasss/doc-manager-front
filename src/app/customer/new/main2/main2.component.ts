import { Component, OnInit, Input } from '@angular/core';
import { DocbackService } from '../../../services/docback.service';

@Component({
  selector: 'app-main2-cust',
  templateUrl: './main2.component.html',
  styleUrls: ['./main2.component.scss']
})
export class Main2CustomerComponent implements OnInit {
  statuses = ['Новый', 'Действующий'];

  constructor(public docBack: DocbackService) { }

  ngOnInit(): void {
    if (!('main2' in this.docBack.localData)) {
      this.docBack.localData.main2 = {};
    }
  }

}
