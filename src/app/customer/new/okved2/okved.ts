export interface Okved {
  id?: number;
  okved_id: number;
  start_date: Date;
  end_date?: Date;
}
