import {AfterViewInit, Component, Inject, Input, OnInit, Optional, ViewChild} from '@angular/core';
import { DocbackService } from '../../../services/docback.service';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {Customer} from '../../customer';
import { Okved } from './okved';
import {MatPaginator} from '@angular/material/paginator';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {DatePipe} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {AddCustomerComponent} from '../../add/add.component';
import {StaffDialogContent} from '../staff/staff.component';

@Component({
  selector: 'app-okved2-cust',
  templateUrl: './okved2.component.html',
  styleUrls: ['./okved2.component.scss']
})
export class Okved2CustomerComponent implements OnInit, AfterViewInit {
  @ViewChild('mainTable', { static: true }) tableMain: MatTable<any> = Object.create(null);
  @ViewChild('otherTable', { static: true }) tableOther: MatTable<any> = Object.create(null);
  displayedColumnsMain: string[] = ['#', 'okved', 'name', 'end_date', 'start_date', 'action'];
  displayedColumnsOther: string[] = ['#', 'okved', 'name', 'end_date', 'start_date', 'action'];
  dataSourceMain: MatTableDataSource<Okved> = new MatTableDataSource<Okved>([]);
  dataSourceOther: MatTableDataSource<Okved> = new MatTableDataSource<Okved>([]);
  @ViewChild('paginatorMain', { static: true }) paginatorMain: MatPaginator = Object.create(null);
  @ViewChild('paginatorOther', { static: true }) paginatorOther: MatPaginator = Object.create(null);
  ngAfterViewInit(): void {
    this.dataSourceMain.paginator = this.paginatorMain;
    this.dataSourceOther.paginator = this.paginatorOther;
  }

  applyFilterMain(filterValue: string): void {
    this.dataSourceMain.filter = filterValue.trim().toLowerCase();
  }
  applyFilterOther(filterValue: string): void {
    this.dataSourceOther.filter = filterValue.trim().toLowerCase();
  }

  constructor(public dialog: MatDialog, public datePipe: DatePipe, private docBack: DocbackService, translate: TranslateService) {
    translate.setDefaultLang('en');
    translate.use('ru');
  }

  ngOnInit(): void {
    if (!('mainOkveds' in this.docBack.localData)) {
      this.docBack.localData.mainOkveds = [];
    }
    if (!('otherOkveds' in this.docBack.localData)) {
      this.docBack.localData.otherOkveds = [];
    }
    this.dataSourceMain.data = this.docBack.localData.mainOkveds;
    this.dataSourceOther.data = this.docBack.localData.otherOkveds;
  }


  openDialogMain(action: string, obj: any): void {
    obj.action = action;
    const dialogRef = this.dialog.open(MainOkvedDialogContent, {
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.event === 'Add') {
        this.addRowDataMain(result.data);
      } else if (result.event === 'Update') {
        this.updateRowDataMain(result.data);
      } else if (result.event === 'Delete') {
        this.deleteRowDataMain(result.data);
      } else if (result.event === 'DUP') {
        this.docBack.openSnackBar('Такой ОКВЕД уже есть');
      }
    });
  }
  openDialogOther(action: string, obj: any): void {
    obj.action = action;
    // obj.id = 'new_' + (this.docBack.localData.otherOkveds.length + 1);
    const dialogRef = this.dialog.open(OtherOkvedDialogContent, {
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.event === 'Add') {
        this.addRowDataOther(result.data);
      } else if (result.event === 'Update') {
        this.updateRowDataOther(result.data);
      } else if (result.event === 'Delete') {
        this.deleteRowDataOther(result.data);
      } else if (result.event === 'DUP') {
        this.docBack.openSnackBar('Такой ОКВЕД уже есть');
      }
    });
  }
  okvedFind(id: number): any {
    return this.docBack.okveds.find((value: any) => value.id === id);
  }
  addRowDataMain(rowObj: any): void {
    this.docBack.localData.mainOkveds.push(rowObj);
    this.tableMain.renderRows();
    this.dataSourceMain.paginator = this.paginatorMain;
  }
  addRowDataOther(rowObj: any): void {
    this.docBack.localData.otherOkveds.push(rowObj);
    this.tableOther.renderRows();
    this.dataSourceOther.paginator = this.paginatorMain;
  }
  updateRowDataMain(rowObj: any): boolean | any {
    this.docBack.localData.mainOkveds = [];
    rowObj.action = 'Update';
    this.docBack.localData.mainOkveds.push(rowObj);
    this.dataSourceMain.data = this.docBack.localData.mainOkveds;
    this.tableMain.renderRows();
    this.dataSourceMain.paginator = this.paginatorMain;
  }
  updateRowDataOther(rowObj: any): boolean | any {
    const updatedIndex = this.docBack.localData.otherOkveds.findIndex((value: any) => {
      return value.okved_id === rowObj.okved_id && value.type === rowObj.type && value.date_start === rowObj.date_start;
    });
    rowObj.action = 'Update';
    this.docBack.localData.otherOkveds[updatedIndex] = rowObj;
    this.dataSourceOther.data = this.docBack.localData.otherOkveds;
    this.tableOther.renderRows();
    this.dataSourceOther.paginator = this.paginatorOther;
  }
  deleteRowDataMain(rowObj: Customer): boolean | any {
    this.docBack.localData.mainOkveds = [];
    this.dataSourceMain.data = this.docBack.localData.mainOkveds;
    this.tableMain.renderRows();
    this.dataSourceMain.paginator = this.paginatorMain;
  }
  deleteRowDataOther(rowObj: any): boolean | any {
    console.log(rowObj);
    const okvedForDelete = this.docBack.localData.otherOkveds.find((value: any) => {
        return value.okved_id === rowObj.okved_id;
      }
    );
    okvedForDelete.action = 'Delete';
    this.docBack.localData.otherOkveds = this.dataSourceOther.data;
  }


}

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'main-okved-dialog-content',
  templateUrl: 'main-okved-dialog-content.html',
})
// tslint:disable-next-line: component-class-suffix
export class MainOkvedDialogContent {
  action: string;
  // tslint:disable-next-line - Disables all
  local_data: any;
  // selectedImage: any = '';
  // joiningDate: any = '';
  userForm: any;
  // staffStruct: any = [];

  constructor(
    public docBack: DocbackService,
    public datePipe: DatePipe,
    public dialogRef: MatDialogRef<MainOkvedDialogContent>,
    // @Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Customer) {
    this.local_data = { ...data };
    this.action = this.local_data.action;
  }

  doAction(): void {
    if (this.docBack.localData.otherOkveds.find((value: any) => value.okved_id === this.local_data.okved_id)) {
      this.dialogRef.close({ event: 'DUP' });
    } else {
      this.dialogRef.close({event: this.action, data: this.local_data});
    }
  }
  closeDialog(): void {
    this.dialogRef.close({ event: 'Cancel' });
  }
  clickEd(event: any): void {
    console.log(event);
    event.target.firstChild.click();
  }

  // selectFile(event: any): void {
  //     if (!event.target.files[0] || event.target.files[0].length === 0) {
  //         // this.msg = 'You must select an image';
  //         return;
  //     }
  //     const mimeType = event.target.files[0].type;
  //     if (mimeType.match(/image\/*/) == null) {
  //         // this.msg = "Only images are supported";
  //         return;
  //     }
  //     // tslint:disable-next-line - Disables all
  //     const reader = new FileReader();
  //     reader.readAsDataURL(event.target.files[0]);
  //     // tslint:disable-next-line - Disables all
  //     reader.onload = (_event) => {
  //         // tslint:disable-next-line - Disables all
  //         this.local_data.imagePath = reader.result;
  //     };
  // }

}

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'other-okved-dialog-content',
  templateUrl: 'other-okved-dialog-content.html',
})
// tslint:disable-next-line: component-class-suffix
export class OtherOkvedDialogContent {
  action: string;
  // tslint:disable-next-line - Disables all
  local_data: any;
  userForm: any;

  constructor(
    public docBack: DocbackService,
    public datePipe: DatePipe,
    public dialogRef: MatDialogRef<OtherOkvedDialogContent>,
    // @Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Customer) {
    this.local_data = { ...data };
    this.action = this.local_data.action;
  }

  doAction(): void {
    if (this.docBack.localData.mainOkveds.find((value: any) => value.okved_id === this.local_data.okved_id)) {
      this.dialogRef.close({ event: 'DUP' });
    } else {
      this.dialogRef.close({event: this.action, data: this.local_data});
    }
  }
  closeDialog(): void {
    this.dialogRef.close({ event: 'Cancel' });
  }
  clickEd(event: any): void {
    console.log(event);
    event.target.firstChild.click();
  }

  // selectFile(event: any): void {
  //     if (!event.target.files[0] || event.target.files[0].length === 0) {
  //         // this.msg = 'You must select an image';
  //         return;
  //     }
  //     const mimeType = event.target.files[0].type;
  //     if (mimeType.match(/image\/*/) == null) {
  //         // this.msg = "Only images are supported";
  //         return;
  //     }
  //     // tslint:disable-next-line - Disables all
  //     const reader = new FileReader();
  //     reader.readAsDataURL(event.target.files[0]);
  //     // tslint:disable-next-line - Disables all
  //     reader.onload = (_event) => {
  //         // tslint:disable-next-line - Disables all
  //         this.local_data.imagePath = reader.result;
  //     };
  // }

}

