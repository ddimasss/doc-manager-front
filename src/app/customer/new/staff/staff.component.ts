import {Component, OnInit, ViewChild, AfterViewInit, Optional, Inject, Input} from '@angular/core';
import {DocbackService, Tariff} from '../../../services/docback.service';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {Customer} from '../../customer';
import {MatPaginator} from '@angular/material/paginator';
import { CustomerDialogContent } from '../../customer.component';
import {AddCustomerComponent} from '../../add/add.component';
import {DatePipe} from '@angular/common';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-staff-cust',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss']
})
export class StaffCustomerComponent implements OnInit, AfterViewInit {
  @ViewChild(MatTable, { static: true }) table: MatTable<any> = Object.create(null);
  searchText: any;
  displayedColumns: string[] = ['#', 'f_name', 's_name', 'l_name_i', 'position', 'ci_end_date', 'sdl_start_date', 'action'];
  dataSource: MatTableDataSource<Customer> = new MatTableDataSource<Customer>([]);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator = Object.create(null);
  customers: Customer[] = [];

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public dialog: MatDialog, public datePipe: DatePipe, private docBack: DocbackService, translate: TranslateService) {
    translate.setDefaultLang('en');
    translate.use('ru');
  }

  ngOnInit(): void {
    if (!('staff' in this.docBack.localData) || this.docBack.localData.staff === null) {
      this.docBack.localData.staff = [];
    }
    this.dataSource.data = this.docBack.localData.staff;
  }
  filt(id: number): string {
    return this.docBack.localData.staffstruct.find((data: any) => data.id === id).name_i;
  }


  openDialog(action: string, obj: any): void {
    obj.action = action;
    if (!obj.id) {
      obj.id = 'new_' + (this.docBack.localData.staff.length + 1);
    }
    if (!('staffstruct' in this.docBack.localData)) {
      obj.staffstruct = [];
    } else {
      obj.staffstruct = this.docBack.localData.staffstruct;
    }
    const dialogRef = this.dialog.open(StaffDialogContent, {
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.event === 'Add') {
        this.addRowData(result.data);
      } else if (result.event === 'Update') {
        this.updateRowData(result.data);
      } else if (result.event === 'Delete') {
        this.deleteRowData(result.data);
      }
    });
  }
  addRowData(rowObj: Customer): void {
    this.docBack.localData.staff.push(rowObj);
    this.dataSource.data = this.docBack.localData.staff;
    // this.dialog.open(AddCustomerComponent);
    this.table.renderRows();
    this.dataSource.paginator = this.paginator;
  }
  updateRowData(rowObj: any): boolean | any {
    const updatedIndex = this.docBack.localData.staff.findIndex((value: any) => {
      return value.id === rowObj.id;
    });
    rowObj.action = 'Update';
    this.docBack.localData.staff[updatedIndex] = rowObj;
    this.dataSource.data = this.docBack.localData.staff;
    this.table.renderRows();
    this.dataSource.paginator = this.paginator;
  }
  deleteRowData(rowObj: any): boolean | any {
    const deletedStaff = this.docBack.localData.staff.find((value: any) => {
      return value.id === rowObj.id;
    });
    deletedStaff.action = 'Delete';
    this.dataSource.data = this.docBack.localData.staff;
    // this.dialog.open(AddCustomerComponent);
    this.table.renderRows();
    this.dataSource.paginator = this.paginator;
  }

}



@Component({
  // tslint:disable-next-line: component-selector
  selector: 'stuff-dialog-content',
  templateUrl: 'stuff-dialog-content.html',
})
// tslint:disable-next-line: component-class-suffix
export class StaffDialogContent {
  action: string;
  // tslint:disable-next-line - Disables all
  local_data: any;
  selectedImage: any = '';
  joiningDate: any = '';
  userForm: any;
  staffStruct: any = [];

  constructor(
    public docBack: DocbackService,
    public datePipe: DatePipe,
    public dialogRef: MatDialogRef<StaffDialogContent>,
    // @Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Customer) {
    this.local_data = { ...data };
    this.action = this.local_data.action;
    if (this.local_data.DateOfJoining !== undefined) {
      this.joiningDate = this.datePipe.transform(new Date(this.local_data.DateOfJoining), 'yyyy-MM-dd');
    }
    if (this.local_data.imagePath === undefined) {
      this.local_data.imagePath = 'assets/images/users/default.png';
    }
    // this.docBack.getStaffStruct(0, 100000, 1).subscribe(
    //   (staffStruct: any) => {
    //     this.staffStruct = staffStruct;
    //   }
    // );
  }

  doAction(): void {
    delete this.local_data.staffstruct;
    this.dialogRef.close({ event: this.action, data: this.local_data });
  }
  closeDialog(): void {
    this.dialogRef.close({ event: 'Cancel' });
  }
  clickEd(event: any): void {
    event.target.firstChild.click();
  }

  // selectFile(event: any): void {
  //     if (!event.target.files[0] || event.target.files[0].length === 0) {
  //         // this.msg = 'You must select an image';
  //         return;
  //     }
  //     const mimeType = event.target.files[0].type;
  //     if (mimeType.match(/image\/*/) == null) {
  //         // this.msg = "Only images are supported";
  //         return;
  //     }
  //     // tslint:disable-next-line - Disables all
  //     const reader = new FileReader();
  //     reader.readAsDataURL(event.target.files[0]);
  //     // tslint:disable-next-line - Disables all
  //     reader.onload = (_event) => {
  //         // tslint:disable-next-line - Disables all
  //         this.local_data.imagePath = reader.result;
  //     };
  // }

}

