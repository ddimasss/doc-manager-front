import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DocbackService} from '../../services/docback.service';
import { HttpHeaders } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import {
  checkINN
  // checkSnils,
  // checkOgrn,
  // checkOgrnip,
  // checkBik,
  // checkSnilsOnlyChecksum
} from 'ru-validation-codes';

@Component({
  selector: 'app-add',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewCustomerComponent implements OnInit {
  id: any = 'new';
  buttonEnabled = true;
  isLoading = false;
  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
    public docBack: DocbackService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {

  }
  openSnackBar(text: string): void {
    this.snackBar.open(text, 'Error', {
      duration: 25 * 1000,
      panelClass: ['style-error']
    });
  }
  checkMain(): boolean {
    if (!('itn' in this.docBack.localData.main)) {
      this.openSnackBar('Заполните ИНН в основных сведениях');
      return false;
    }
    if (!checkINN(this.docBack.localData.main.itn)) {
      this.openSnackBar('ИНН заполнен не правильно');
      return false;
    }
    if (!('kpp' in this.docBack.localData.main)) {
      this.openSnackBar('Заполните КПП в основных сведениях');
      return false;
    }
    if (!('ogrn' in this.docBack.localData.main)) {
      this.openSnackBar('Заполните ОГРН в основных сведениях');
      return false;
    }
    if (!('timezone' in this.docBack.localData.main)) {
      this.openSnackBar('Заполните Временную зону в основных сведениях');
      return false;
    }
    if (!('company_type_id' in this.docBack.localData.main)) {
      this.openSnackBar('Заполните Форму собственности в основных сведениях');
      return false;
    }
    if (!('company_type_id' in this.docBack.localData.main)) {
      this.openSnackBar('Заполните Форму собственности в основных сведениях');
      return false;
    }
    if (!('customer_categories' in this.docBack.localData.main) || this.docBack.localData.main.customer_categories.length < 1) {
      this.openSnackBar(' Выберите категории клиента в основных сведениях');
      return false;
    }
    if (!('full_name' in this.docBack.localData.main)) {
      this.openSnackBar('Заполните Полное наименование в основных сведениях');
      return false;
    }
    if (!('short_name' in this.docBack.localData.main)) {
      this.openSnackBar('Заполните Краткое наименование в основных сведениях');
      return false;
    }
    if (!('phone' in this.docBack.localData.main)) {
      this.openSnackBar('Заполните Телефон организации в основных сведениях');
      return false;
    }
    if (!('email' in this.docBack.localData.main)) {
      this.openSnackBar('Заполните Email организации в основных сведениях');
      return false;
    }
    let ur = 0;
    let fac = 0;
    let poc = 0;
    this.docBack.localData.main.addresses.forEach((value: any) => {
      if (value.type === 'LEGAL') {
        ur = 1;
      }
      if (value.type === 'MAILING') {
        poc = 1;
      }
      if (value.type === 'ACTUAL') {
        fac = 1;
      }
    });
    if (!(ur)) {
      this.openSnackBar('Заполните Юридический адрес и дату начала в основных сведениях');
      return false;
    }
    if (!(fac)) {
      this.openSnackBar('Заполните Фактический адрес и дату начала в основных сведениях');
      return false;
    }
    if (!(poc)) {
      this.openSnackBar('Заполните Почтовый адрес и дату начала в основных сведениях');
      return false;
    }
    return true;
  }
  checkMain2(): boolean {
    if (!('main2' in this.docBack.localData)) {
      this.openSnackBar('Заполните дополнительные сведения');
    }
    // tslint:disable-next-line:max-line-length
    if (!('contract_number' in this.docBack.localData.main2) || !('contract_date_start' in this.docBack.localData.main2) || !('contract_date_end' in this.docBack.localData.main2)) {
      this.openSnackBar('Заполните Номер договора и даты в дополнительных сведениях');
      return false;
    }
    if (!('tariff_id' in this.docBack.localData.main2)) {
      this.openSnackBar('Выберите тариф в дополнительных сведениях');
      return false;
    }
    return true;
  }
  checkOkved(): boolean {
    if (!('mainOkveds' in this.docBack.localData)) {
      this.openSnackBar('Заполните основной ОКВЭД в ОКВЭД2');
      return false;
    }
    if (!('otherOkveds' in this.docBack.localData)) {
      this.openSnackBar('Заполните дополнительные ОКВЭДы в ОКВЭД2');
      return false;
    }
    if (this.docBack.localData.mainOkveds.length === 0) {
      this.openSnackBar('Заполните основной ОКВЭД в ОКВЭД2');
      return false;
    }
    if (this.docBack.localData.otherOkveds.length === 0) {
      this.openSnackBar('Заполните дополнительные ОКВЭДы в ОКВЭД2');
      return false;
    }
    return true;
  }
  checkStaffStruct(): boolean {
    if (!('staffstruct' in this.docBack.localData) || this.docBack.localData.staffstruct.length === 0) {
      this.openSnackBar('Добавьте минимум одну должность в Штатную структуру');
      return false;
    }
    return true;
  }
  checkStaff(): boolean {
    if (!('staff' in this.docBack.localData) || this.docBack.localData.staff.length === 0) {
      this.openSnackBar('Добавьте минимум одного сотрудника в Сотрудники');
      return false;
    }
    return true;
  }

  checkInput(): boolean {
    if (!this.checkMain()) {
      return false;
    }
    if (!this.checkMain2()) {
      return false;
    }
    if (!this.checkOkved()) {
      return false;
    }
    if (!this.checkStaffStruct()) {
      return false;
    }
    return this.checkStaff();
  }
  saveCustomer(): void {
    this.buttonEnabled = false;
    setTimeout(() => {                           // <<<---using ()=> syntax
      this.buttonEnabled = true;
    }, 3000);
    this.isLoading = true;
    console.log(this.docBack.localData);
    if (this.checkInput()) {
      if (this.docBack.localData.action === 'Update') {
        this.updateCustomer();
      } else {
        this.createCustomer();
      }
    } else {
      this.buttonEnabled = true;
      this.isLoading = false;
    }
  }

  updateCustomer(): void {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-User': this.docBack.currentUser ? this.docBack.currentUser.email : '',
      })
    };
    const href = `${this.docBack.baseUrl}customer/update`;
    this.docBack.localData.copy = this.docBack.localDataCopy;
    this.http.post(href, this.docBack.localData, httpOptions).subscribe(
      (resp: any) => {
        this.buttonEnabled = true;
        if (resp.result === 'error') {
          this.openSnackBar('Ошибка: ' + resp.message);
          this.isLoading = false;
        } else {
          this.router.navigate(['/customers']).then();
          this.isLoading = false;
        }
      },
      error => {
        this.buttonEnabled = true;
        this.openSnackBar('Ошибка: ' + error);
        this.isLoading = false;
      }
    );
  }
  createCustomer(): void {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-User': this.docBack.currentUser ? this.docBack.currentUser.email : '',
      })
    };
    const href = `${this.docBack.baseUrl}customer/create`;
    this.http.post(href, this.docBack.localData, httpOptions).subscribe(
      (customer: any) => {
        this.buttonEnabled = true;
        this.isLoading = false;
        if (customer.result === 'error') {
          this.openSnackBar('Ошибка: ' + 'ИНН существует');
        } else {
          this.router.navigate(['/customers']).then();
        }
      },
      error => {
          this.openSnackBar('Ошибка: ' + error.toString());
          this.buttonEnabled = true;
          this.isLoading = false;
      }
    );
  }
}
