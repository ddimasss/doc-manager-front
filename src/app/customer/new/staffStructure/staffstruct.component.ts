import {Component, OnInit, ViewChild, AfterViewInit, Optional, Inject} from '@angular/core';
import { DocbackService } from '../../../services/docback.service';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {Customer} from '../../customer';
import {MatPaginator} from '@angular/material/paginator';
import {DatePipe} from '@angular/common';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {TranslateService} from '@ngx-translate/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {map, startWith} from 'rxjs/operators';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-staffstruct-cust',
  templateUrl: './staffstruct.component.html',
  styleUrls: ['./staffstruct.component.scss']
})
export class StaffStructCustomerComponent implements OnInit, AfterViewInit {
  @ViewChild(MatTable, { static: true }) table: MatTable<any> = Object.create(null);
  searchText: any;
  displayedColumns: string[] = ['#', 'position', 'start_date', 'end_date', 'ruk', 'c_i', 'action'];
  dataSource: MatTableDataSource<Customer> = new MatTableDataSource<Customer>([]);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator = Object.create(null);
  customers: Customer[] = [];

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(public dialog: MatDialog, public datePipe: DatePipe, private docBack: DocbackService, translate: TranslateService) {
    translate.setDefaultLang('en');
    translate.use('ru');
  }

  ngOnInit(): void {
    if (!('staffstruct' in this.docBack.localData)) {
      this.docBack.localData.staffstruct = [];
    }
    this.dataSource.data = this.docBack.localData.staffstruct;
  }


  openDialog(action: string, obj: any): void {
    obj.action = action;
    if (!obj.id) {
      obj.id = 'new_' + (this.docBack.localData.staffstruct.length + 1);
    }
    const dialogRef = this.dialog.open(StaffStructDialogContent, {
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.event === 'Add') {
        this.addRowData(result.data);
      } else if (result.event === 'Update') {
        this.updateRowData(result.data);
      } else if (result.event === 'Delete') {
        this.deleteRowData(result.data);
      }
    });
  }
  addRowData(rowObj: Customer): void {
    console.log(rowObj);
    this.docBack.localData.staffstruct.push(rowObj);
    this.dataSource.data = this.docBack.localData.staffstruct;
    // this.dialog.open(AddCustomerComponent);
    this.table.renderRows();
    this.dataSource.paginator = this.paginator;
  }
  updateRowData(rowObj: any): boolean | any {
    const updatedIndex = this.docBack.localData.staffstruct.findIndex((value: any) => {
      return value.id === rowObj.id;
    });
    rowObj.action = 'Update';
    this.docBack.localData.staffstruct[updatedIndex] = rowObj;
    this.dataSource.data = this.docBack.localData.staffstruct;
    this.table.renderRows();
    this.dataSource.paginator = this.paginator;
  }
  deleteRowData(rowObj: any): boolean | any {
    const deletedStaffStruct = this.docBack.localData.staffstruct.find((value: any) => {
      return value.id === rowObj.id;
    });
    deletedStaffStruct.action = 'Delete';
    this.dataSource.data = this.docBack.localData.staffstruct;
    this.table.renderRows();
    this.dataSource.paginator = this.paginator;
  }

}



@Component({
  // tslint:disable-next-line: component-selector
  selector: 'stuffstruct-dialog-content',
  templateUrl: 'stuffstruct-dialog-content.html',
})
// tslint:disable-next-line: component-class-suffix
export class StaffStructDialogContent {
  action: string;
  // tslint:disable-next-line - Disables all
  local_data: any;
  selectedImage: any = '';
  joiningDate: any = '';
  userForm: any;
  positions: any;
  filteredOptions: any;
  myControl: FormControl = new FormControl();
  constructor(
    private http: HttpClient,
    public docBack: DocbackService,
    public datePipe: DatePipe,
    public dialogRef: MatDialogRef<StaffStructDialogContent>,
    // @Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Customer) {
    this.local_data = { ...data };
    this.action = this.local_data.action;
    if (this.local_data.DateOfJoining !== undefined) {
      this.joiningDate = this.datePipe.transform(new Date(this.local_data.DateOfJoining), 'yyyy-MM-dd');
    }
    if (this.local_data.imagePath === undefined) {
      this.local_data.imagePath = 'assets/images/users/default.png';
    }
    const href = `${this.docBack.baseUrl}position/get_list`;
    let queryParams = new HttpParams();
    queryParams = queryParams.append('limit', 10000);
    queryParams = queryParams.append('offset', 0);
    queryParams = queryParams.append('unique', 1);
    http.get(href, {
        headers: new HttpHeaders({
        'X-User': this.docBack.currentUser ? this.docBack.currentUser.email : ''}), params: queryParams}).subscribe((res: any) => {
    this.positions = res;
    this.filteredOptions = this.myControl.valueChanges
        .pipe(
          startWith(''),
          map(val => this.filter(val))
        );

  });
  }
  onSelFunc(option: any): void {
    this.local_data.name_i = option.name_i;
    this.local_data.name_r = option.name_r;
    this.local_data.name_d = option.name_d;
    this.local_data.name_v = option.name_v;
    this.local_data.name_t = option.name_t;
  }
  filter(value: string = ''): any[] {
    this.local_data.name_i = value;
    const filterValue = value.toLowerCase();
    return this.positions.filter((position: any) => position.name_i.toLowerCase().includes(filterValue));
  }

  doAction(): void {
    this.dialogRef.close({ event: this.action, data: this.local_data });
  }
  closeDialog(): void {
    this.dialogRef.close({ event: 'Cancel' });
  }
  clickEd(event: any): void {
    console.log(event);
    event.target.firstChild.click();
  }

  // selectFile(event: any): void {
  //     if (!event.target.files[0] || event.target.files[0].length === 0) {
  //         // this.msg = 'You must select an image';
  //         return;
  //     }
  //     const mimeType = event.target.files[0].type;
  //     if (mimeType.match(/image\/*/) == null) {
  //         // this.msg = "Only images are supported";
  //         return;
  //     }
  //     // tslint:disable-next-line - Disables all
  //     const reader = new FileReader();
  //     reader.readAsDataURL(event.target.files[0]);
  //     // tslint:disable-next-line - Disables all
  //     reader.onload = (_event) => {
  //         // tslint:disable-next-line - Disables all
  //         this.local_data.imagePath = reader.result;
  //     };
  // }

}

