import { Component, OnInit, Inject, Optional, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
import { AddCustomerComponent } from './add/add.component';
import { Customer } from './customer';
import { DocbackService } from '../services/docback.service';
import {TranslateService} from '@ngx-translate/core';
import { Router } from '@angular/router';
import {HttpHeaders} from '@angular/common/http';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FormBuilder} from '@angular/forms';

@Component({
    templateUrl: './customer.component.html'
})


export class CustomerComponent implements OnInit, AfterViewInit {
    isLoading = false;
    @ViewChild(MatTable, { static: true }) table: MatTable<any> = Object.create(null);
    searchText: any;
    displayedColumns: string[] = ['#', 'short_name', 'full_name', 'itn', 'company_type_id', 'contract_date', 'time_zone',  'action'];
    dataSource: MatTableDataSource<Customer> = new MatTableDataSource<Customer>([]);
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator = Object.create(null);
    customers: Customer[] = [];
    constructor(private router: Router,
                public dialog: MatDialog,
                public datePipe: DatePipe,
                public docBack: DocbackService,
                private http: HttpClient,
                private snackBar: MatSnackBar,
                translate: TranslateService
    ) {
      translate.setDefaultLang('en');
      translate.use('ru');
    }

    ngOnInit(): void {
      this.docBack.getList<Customer>(0, 100, 'customer').subscribe( data => this.dataSource.data = data);
    }

    ngAfterViewInit(): void {
        this.dataSource.paginator = this.paginator;
    }

    applyFilter(filterValue: string): void {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
    newCust(): void {
      this.docBack.localData = {};
      this.docBack.localData.action = 'Create';
      this.docBack.newCustomerId = 'new';
      this.router.navigateByUrl('/new_customers');
    }

    openDialog(action: string, obj: any): void {
        obj.action = action;
        const dialogRef = this.dialog.open(CustomerDialogContent, {
            data: obj
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result.event === 'Add') {
                this.addRowData(result.data);
            } else if (result.event === 'Update') {
                this.updateRowData();
            } else if (result.event === 'Delete') {
                this.deleteRowData(result.data);
            } else if (result.event === 'Generate') {
              this.generate(result.data);
            }
        });
    }
  openDialogDelete(obj: any): void {
      console.log(obj);
      const dialogRef = this.dialog.open(CustomerDialogDelete, {
        data: obj
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result.event === 'Confirm') {
          this.deleteCustomer(obj);
        }
      });
  }
  openDialogMAny(action: string, obj: any): void {
    obj.action = action;
    const dialogRef = this.dialog.open(CustomerDialogContentMany, {
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.event === 'Generate') {
        console.log('Yep');
        this.generateMany(result.data);
      }
    });
  }
    generate(row: any): void {
      this.isLoading = true;
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'X-User': this.docBack.currentUser ? this.docBack.currentUser.email : '',
        })
      };
      const data = {
        id: row.id,
        date: row.date_start,
        instruction_ids: row.instruction_ids,
        user_id: this.docBack.currentUser ? this.docBack.currentUser.id : -1
      };
      const href = `${this.docBack.baseUrl}customer/generate`;
      this.http.post(href, data, httpOptions).subscribe(
        () => {
          this.isLoading = false;
          this.docBack.openSnackBar('Генерация началась');
        },
        error => {
          this.docBack.openSnackBar('Ошибка: ' + error);
          this.isLoading = false;
        }
      );
    }

  generateMany(row: any): void {
    this.isLoading = true;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-User': this.docBack.currentUser ? this.docBack.currentUser.email : '',
      })
    };
    const data = {
      id: row.customer_category,
      date: row.date,
      instruction_ids: row.instruction_ids,
      user_id: this.docBack.currentUser ? this.docBack.currentUser.id : -1
    };
    const href = `${this.docBack.baseUrl}customer/generate_many`;
    this.http.post(href, data, httpOptions).subscribe(
      () => {
        this.isLoading = false;
        this.docBack.openSnackBar('Генерация началась');
      },
      error => {
        this.docBack.openSnackBar('Ошибка: ' + error);
        this.isLoading = false;
      }
    );
  }
    // tslint:disable-next-line - Disables all
    addRowData(row_obj: Customer): void {
        console.log(row_obj);
        // this.dataSource.data.push({
        //     id: employees.length + 1,
        //     Name: row_obj.Name,
        //     Position: row_obj.Position,
        //     Email: row_obj.Email,
        //     Mobile: row_obj.Mobile,
        //
        //     DateOfJoining: new Date(),
        //     Salary: row_obj.Salary,
        //     Projects: row_obj.Projects,
        //     imagePath: row_obj.imagePath
        // });
        this.dialog.open(AddCustomerComponent);
        this.table.renderRows();
    }
  deleteCustomer(cust: any): void {
    this.isLoading = true;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-User': this.docBack.currentUser ? this.docBack.currentUser.email : '',
      })
    };
    const href = `${this.docBack.baseUrl}customer/delete`;
    this.http.post(href, cust.id, httpOptions).subscribe(
      (resp: any) => {
        if (resp.result === 'error') {
          this.docBack.openSnackBar('Ошибка: ' + resp.message);
          this.isLoading = false;
        } else {
          window.location.reload();
          this.isLoading = false;
        }
      },
      error => {
        this.docBack.openSnackBar('Ошибка: ' + error);
        this.isLoading = false;
      }
    );
  }
    updateCustomer(cust: any): void {
        this.docBack.getCustomerById(cust.id).subscribe(
          data => {
            this.docBack.localData = data;
            this.docBack.localData.action = 'Update';
            if ('staff' in data) {
              if (data.staff !== null) {
                this.docBack.localData.staff.l_name_i = data.staff.l_name;
              }
            }
            this.docBack.newCustomerId = cust.id;
            this.docBack.localDataCopy = JSON.parse(JSON.stringify(this.docBack.localData));
            this.router.navigateByUrl('/new_customers');
          }
        );
    }
    // tslint:disable-next-line - Disables all
    updateRowData(): boolean | any {
        // this.dataSource.data = this.dataSource.data.filter((value: any) => {
        //     if (value.id === row_obj.id) {
        //         value.Name = row_obj.Name;
        //         value.Position = row_obj.Position;
        //         value.Email = row_obj.Email;
        //         value.Mobile = row_obj.Mobile;
        //         value.DateOfJoining = row_obj.DateOfJoining;
        //         value.Salary = row_obj.Salary;
        //         value.Projects = row_obj.Projects;
        //         value.imagePath = row_obj.imagePath;
        //     }
        //     return true;
        // });
    }

    // tslint:disable-next-line - Disables all
    deleteRowData(row_obj: Customer): boolean | any {
        this.dataSource.data = this.dataSource.data.filter((value: any) => {
            return value.id !== row_obj.id;
        });
    }
}


@Component({
    // tslint:disable-next-line: component-selector
    selector: 'dialog-content',
    templateUrl: 'dialog-content.html',
})
// tslint:disable-next-line: component-class-suffix
export class CustomerDialogContent {
    action: string;
    isLoading = false;
    // tslint:disable-next-line - Disables all
    local_data: any;
    selectedImage: any = '';
    joiningDate: any = '';
    isGenerated = false;
    toppings: any[] = [];

    constructor(
        private formBuilder: FormBuilder,
        public docBack: DocbackService,
        public datePipe: DatePipe,
        private http: HttpClient,
        public dialogRef: MatDialogRef<CustomerDialogContent>,
        // @Optional() is used to prevent error if no data is passed
        @Optional() @Inject(MAT_DIALOG_DATA) public data: Customer) {
        this.local_data = { ...data };
        this.action = this.local_data.action;
        if (this.local_data.DateOfJoining !== undefined) {
            this.joiningDate = this.datePipe.transform(new Date(this.local_data.DateOfJoining), 'yyyy-MM-dd');
        }
        if (this.local_data.imagePath === undefined) {
            this.local_data.imagePath = 'assets/images/users/default.png';
        }
    }

    doAction(): void {
        this.dialogRef.close({ event: this.action, data: this.local_data });
    }
    generate(): void {
      this.isLoading = true;
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'X-User': this.docBack.currentUser ? this.docBack.currentUser.email : '',
        })
      };
      const httpData = {
        customer_id: this.local_data.id,
        date_start: this.local_data.date_start,
        date_end: this.local_data.date_end,
        document_types: this.local_data.instruction_ids,
      };
      const href = `${this.docBack.baseUrl}gen/get_documents`;
      this.http.post(href, httpData, httpOptions).subscribe(
        (resp: any) => {
          if (resp.result === 'error') {
            this.docBack.openSnackBar('Ошибка: ' + resp.message);
            this.isLoading = false;
          } else {
            console.log(resp);
            const res: any[] = [];
            for (const templ of resp) {
              const cb: any = {};
              cb.value = templ;
              cb.name = templ[5];
              cb.checked = false;
              res.push(cb);
            }
            this.toppings = res;
            this.isGenerated = true;
            this.isLoading = false;
          }
        },
        error => {
          this.docBack.openSnackBar('Ошибка: ' + error);
          this.isLoading = false;
        }
      );
      // this.dialogRef.close({ event: this.action, data: this.local_data });
    }
    send(): void {
      console.log('send');
      console.log(this.toppings);
      this.dialogRef.close({ event: this.action, data: this.local_data });
    }
    closeDialog(): void {
        this.dialogRef.close({ event: 'Cancel' });
    }
    clickEd(event: any): void {
      console.log(event);
      event.target.firstChild.click();
    }
}

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'dialog-content-many',
  templateUrl: 'dialog-content-many.html',
})
// tslint:disable-next-line: component-class-suffix
export class CustomerDialogContentMany {
  action: string;
  // tslint:disable-next-line - Disables all
  local_data: any;
  selectedImage: any = '';
  joiningDate: any = '';

  constructor(
    public docBack: DocbackService,
    public datePipe: DatePipe,
    public dialogRef: MatDialogRef<CustomerDialogContentMany>,
    // @Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Customer) {
    this.local_data = { ...data };
    this.action = this.local_data.action;
    if (this.local_data.DateOfJoining !== undefined) {
      this.joiningDate = this.datePipe.transform(new Date(this.local_data.DateOfJoining), 'yyyy-MM-dd');
    }
    if (this.local_data.imagePath === undefined) {
      this.local_data.imagePath = 'assets/images/users/default.png';
    }
  }

  doAction(): void {
    this.dialogRef.close({ event: this.action, data: this.local_data });
  }
  closeDialog(): void {
    this.dialogRef.close({ event: 'Cancel' });
  }
  clickEd(event: any): void {
    console.log(event);
    event.target.firstChild.click();
  }
}

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'dialog-delete',
  templateUrl: 'dialog-delete.html',
})
// tslint:disable-next-line: component-class-suffix
export class CustomerDialogDelete {
  customer: any;

  constructor(
    public docBack: DocbackService,
    public datePipe: DatePipe,
    public dialogRef: MatDialogRef<CustomerDialogContentMany>,
    // @Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Customer) {
    this.customer = { ...data };
  }

  doAction(): void {
    this.dialogRef.close({ event: 'Confirm' });
  }
  closeDialog(): void {
    this.dialogRef.close({ event: 'Cancel' });
  }
}
