export interface Customer {
    id?: number;
    itn: string;
    company_type_id: number;
    full_name: string;
    time_zone: number;
    contract_date: Date;
    short_name: string;
    ogrn?: string;
    kpp?: string;
    ur_address: string;
    fact_address?: string;
    post_address?: string;
    phone?: string;
    is_deleted?: boolean;
    main_okved_id: number;
    description?: string;
    tariff_id: number;
    contact_name: string;
    contact_phone: string;
}
export interface Address {
  id?: number;
  address: string;
  date_start: Date;
  date_end: Date;
  is_deleted: boolean;
  type: string;
  customer_id: number;
}

// id: Optional[int] = None
// itn: str
// company_type_id: int
// full_name: str
// time_zone: int
// contract_date: date
// short_name: str
// ogrn: Optional[str]
// kpp: Optional[str]
// ur_address: str
// fact_address: Optional[str]
// post_address: Optional[str]
// phone: Optional[str]
// is_deleted: Optional[bool] = False
// main_okved_id: int
// description: Optional[str] = None
// tariff_id: int
// contact_name: str
// contact_phone: str
// customer_category_id: List[int]
