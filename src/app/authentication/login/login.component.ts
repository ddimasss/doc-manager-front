import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DocbackService } from '../../services/docback.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { User } from '../../users/user';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email = '';
  password = '';
  constructor(
    private router: Router,
    public docBack: DocbackService,
    private http: HttpClient,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {}

  onSubmit(): void {
    console.log(this.email + ' - ' + this.password);
    const href = `${this.docBack.baseUrl}users/login`;
    this.http.post<User>(href, {email: this.email, password: this.password}, {headers: new HttpHeaders({
      'X-User': this.email ? this.email : ''})}).subscribe(
      (user: User) => {
        this.docBack.currentUser = user;
        localStorage.setItem('currentUser', JSON.stringify(this.docBack.currentUser));
        this.router.navigate(['/customers']);
      },
      () => {
        this.docBack.currentUser = null;
        this.snackBar.open('Ошибка в почте или пароле!', undefined, {duration: 2000, panelClass: ['red-snackbar']});
      }
    );
  }
}
