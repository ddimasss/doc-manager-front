import { Component, OnInit, Inject, Optional, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
import { AddTemplateComponent } from './add/add.component';
import { Template } from './template';
import { DocbackService } from '../services/docback.service';
import {TranslateService} from '@ngx-translate/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
    templateUrl: './template.component.html'
})


export class TemplateComponent implements OnInit, AfterViewInit {
    @ViewChild(MatTable, { static: true }) table: MatTable<any> = Object.create(null);
    searchText: any;
    displayedColumns: string[] = ['#', 'name', 'employee_count', 'file_path', 'message', 'doc_start_date', 'gen_start_date', 'npa_name', 'npa_date', 'is_deleted', 'instruction_id',   'action'];
    dataSource: MatTableDataSource<Template> = new MatTableDataSource<Template>([]);
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator = Object.create(null);
    customers: Template[] = [];
    constructor(
      public dialog: MatDialog,
      public datePipe: DatePipe,
      private router: Router,
      public docBack: DocbackService,
      translate: TranslateService) {
      translate.setDefaultLang('en');
      translate.use('ru');
    }

    ngOnInit(): void {
      this.docBack.getList<Template>(0, 10000, 'templates').subscribe(data => {
        this.dataSource.data = data;
        console.log(data);
      });
    }

    ngAfterViewInit(): void {
        this.dataSource.paginator = this.paginator;
    }

    applyFilter(filterValue: string): void {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    openDialog(action: string, obj: any): void {
        obj.action = action;
        const dialogRef = this.dialog.open(TemplateDialogContent, {
            data: obj
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            if (result.event === 'Add') {
              this.addRowData();
            } else if (result.event === 'Update') {
              this.updateRowData();
            } else if (result.event === 'Delete') {
              this.deleteRowData();
            } else if (result.event === 'Error') {
              this.dialog.open(AddTemplateComponent);
            }
          }
        });
    }
    addRowData(): void {
      window.location.reload();
    }

    // tslint:disable-next-line - Disables all
    updateRowData(): boolean | any {
      window.location.reload();
        // this.dataSource.data = this.dataSource.data.filter((value: any) => {
        //     if (value.id === row_obj.id) {
        //         value.Name = row_obj.Name;
        //         value.Position = row_obj.Position;
        //         value.Email = row_obj.Email;
        //         value.Mobile = row_obj.Mobile;
        //         value.DateOfJoining = row_obj.DateOfJoining;
        //         value.Salary = row_obj.Salary;
        //         value.Projects = row_obj.Projects;
        //         value.imagePath = row_obj.imagePath;
        //     }
        //     return true;
        // });
    }

    // tslint:disable-next-line - Disables all
    deleteRowData(): boolean | any {
      window.location.reload();
    }
}


@Component({
    // tslint:disable-next-line: component-selector
    selector: 'dialog-content',
    templateUrl: 'dialog-content.html',
    styleUrls: ['./dialog.component.scss']
})
// tslint:disable-next-line: component-class-suffix
export class TemplateDialogContent {
    action: string;
    // tslint:disable-next-line - Disables all
    local_data: any;
    local_data_copy: any;
    file: any;
    joiningDate: any = '';
    formData = new FormData();
    constructor(
        private http: HttpClient,
        public docBack: DocbackService,
        public datePipe: DatePipe,
        public dialogRef: MatDialogRef<TemplateDialogContent>,
        // @Optional() is used to prevent error if no data is passed
        @Optional() @Inject(MAT_DIALOG_DATA) public data: Template
    ) {
        this.local_data = { ...data };
        this.local_data_copy = JSON.stringify(this.local_data);
        this.action = this.local_data.action;
        if (this.local_data.DateOfJoining !== undefined) {
            this.joiningDate = this.datePipe.transform(new Date(this.local_data.DateOfJoining), 'yyyy-MM-dd');
        }
    }
    log1(a: string): void {
      console.log(a);
    }

    doAction(): void {
      if (this.local_data.action === 'Delete') {
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'X-User': this.docBack.currentUser ? this.docBack.currentUser.email : '',
          })
        };
        const href = `${this.docBack.baseUrl}templates/delete`;
        const body = {template_id: this.local_data.id};
        this.http.post(href, body, httpOptions).subscribe(
          template => {
            this.dialogRef.close({event: this.action, data: template});
          },
          error => {
            this.dialogRef.close({event: 'Error', data: error});
          }
        );
      } else {
        this.formData = new FormData();
        const href = `${this.docBack.baseUrl}templates/edit_upload`;
        this.formData.append('local_data', JSON.stringify(this.local_data));
        this.formData.append('local_data_copy', this.local_data_copy);
        if ('file' in this) {
          this.formData.append('file', this.file, this.file.name);
        }
        this.http.post(href, this.formData, {headers: {'X-User': this.docBack.currentUser ? this.docBack.currentUser.email : ''}})
          .subscribe(
          template => {
            this.dialogRef.close({event: this.action, data: template});
          },
          error => {
            this.dialogRef.close({event: 'Error', data: error});
          }
        );
      }
    }
    closeDialog(): void {
        this.dialogRef.close({ event: 'Cancel' });
    }
    clickEd(event: any): void {
      console.log(event);
      event.target.firstChild.click();
    }

    selectFile(event: any): void {
        if (!event.target.files[0] || event.target.files[0].length === 0) {
            // this.msg = 'You must select a file';
            return;
        }
        const file: File = event.target.files[0];
        if (file) {
          this.file = file;
          this.local_data.file_path = file.name;
        }
        // const reader = new FileReader();
        // reader.readAsDataURL(event.target.files[0]);
        // // tslint:disable-next-line - Disables all
        // reader.onload = (_event) => {
        //     // tslint:disable-next-line - Disables all
        //     this.local_data.imagePath = reader.result;
        // };
    }

}
