import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { UserActions, desc } from './UserActions';
import { DocbackService } from '../services/docback.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
    templateUrl: './user-actions.component.html'
})


export class UserActionsComponent implements OnInit, AfterViewInit {
    @ViewChild(MatTable, { static: true }) table: MatTable<any> = Object.create(null);
    searchText: any;
    displayedColumns: string[] = ['#', 'user_email', 'created_at', 'action', 'old_data', 'new_data'];
    dataSource: MatTableDataSource<UserActions> = new MatTableDataSource<UserActions>([]);
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator = Object.create(null);
    settings: UserActions[] = [];
    constructor(
      public docBack: DocbackService,
      translate: TranslateService
    ) {
      translate.setDefaultLang('en');
      translate.use('ru');
    }

    ngOnInit(): void {
      this.docBack.getList<UserActions>(0, 10000, 'send_logs/get_list_ua').subscribe(data => {
        data.forEach((obj) => {
          const pathWoPrefix = obj.endpoint.slice(9);
          const pathArr = pathWoPrefix.split('/');
          const path = pathArr[0] + '/' + pathArr[1];
          // @ts-ignore
          const res: string = desc.get(path) !== undefined ? desc.get(path) : '';
          console.log();
          obj.endpoint = res;
        });
        this.dataSource.data = data;
      });
    }
    objPrepare(obj: any): string {
      return JSON.stringify(obj)
    }

    ngAfterViewInit(): void {
        this.dataSource.paginator = this.paginator;
    }

    applyFilter(filterValue: string): void {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
}
