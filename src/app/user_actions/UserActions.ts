export interface UserActions {
    id?: number;
    user_email: string;
    created_at: Date;
    endpoint: string;
    old_data?: any;
    new_data?: any;
}
export let desc = new Map([
  ['send_logs/get_list_ua', 'Просмотр журнала действий пользователя'],
  ['tariff/get_list', 'Просмотр тарифов'],
  ['roles/get_list', 'Просмотр ролей'],
  ['instruction/get_list', 'Просмотр типов инструктажей'],
  ['gen/get_documents', 'Просмотр документов доступных для генерации'],
  ['company_type/get_list', 'Просмотр списка типов организаций'],
  ['company_type/get', 'Просмотр организации'],
  ['company_type/edit', 'Редактирование организации'],
  ['company_type/add', 'Добавление организации'],
  ['okved/get_list', 'Просмотр списка ОКВЭД'],
  ['okved/get', 'Просмотр ОКВЭД'],
  ['okved/edit', 'Редактирование ОКВЭД'],
  ['okved/add', 'Добавление ОКВЭД'],
  ['message/get_list', 'Просмотр списка сообщений'],
  ['message/get', 'Просмотр сообщения'],
  ['message/add', 'Добавление сообщения'],
  ['message/edit', 'Редактирование сообщения'],
  ['customer/get_list', 'Просмотр списка клиентов'],
  ['customer/get', 'Просмотр клиента'],
  ['customer/get_all', 'Просмотр всех данных клиента'],
  ['customer/get_stuff_struct', 'Просмотр организационной структуры клиента'],
  ['customer/create', 'Добавление клиента'],
  ['customer/delete', 'Удаление клиента'],
  ['customer/update', 'Редактирование клиента'],
  ['customer/generate', 'Генерация документов для клиента'],
  ['customer/generate_many', 'Генерация документов для списка клиентов'],
  ['employee/get_list', 'Просмотр списка сотрудников'],
  ['employee/get', 'Просмотр сотрудника'],
  ['employee/add', 'Добавление сотрудника'],
  ['employee/edit', 'Редактирование сотрудника'],
  ['instruction/get_list', 'Просмотр списка документов'],
  ['instruction/get', 'Просмотр документа'],
  ['instruction/add', 'Добавление документа'],
  ['instruction/edit', 'Редактирование документа'],
  ['position/get_list', 'Просмотр списка должностей'],
  ['position/get', 'Просмотр должности'],
  ['position/add', 'Добавление должности'],
  ['position/edit', 'Редактирование должности'],
  ['settings/get_list', 'Просмотр списка настроек'],
  ['settings/get', 'Просмотр настройки'],
  ['settings/add', 'Добавление настройки'],
  ['settings/edit', 'Редактирование настройки'],
  ['templates/get_list', 'Просмотр списка шаблонов'],
  ['templates/get', 'Просмотр шаблона'],
  ['templates/add', 'Добавление шаблона'],
  ['templates/edit', 'Редактирование шаблона'],
  ['templates/delete', 'Удаление шаблона'],
  ['templates/upload', 'Добавление шаблона'],
  ['templates/edit_upload', 'Редактирование шаблона'],
  ['customer_category/get_list', 'Просмотр списка категорий клиентов'],
  ['customer_category/get', 'Просмотр категории клиентов'],
  ['customer_category/add', 'Добавление категории клиентов'],
  ['customer_category/edit', 'Редактирование категории клиентов'],
  ['send_logs/get_list', 'Просмотр записей логирования отправки'],
  ['send_logs/get', 'Просмотр лога отправки'],
  ['send_logs/add', 'Добавление лога отправки'],
  ['send_logs/edit', 'Редактирование лога отправки'],
  ['send_logs/get_xlsx', 'Загрука Excel лога отправки'],
  ['send_logs/get_xlsx_ua', 'Загрука Excel лога действий пользователя'],
  ['send_logs/get_list_ua', 'Просмотр списка действий пользователя'],
  ['users/get_list', 'Просмотр списка пользователей'],
  ['users/get', 'Просмотр пользователя'],
  ['users/add', 'Добавление пользователя'],
  ['users/edit', 'Редактирование пользователя'],
  ['roles/get_list', 'Просмотр списка ролей'],
  ['roles/add', 'Добавление роли'],
  ['roles/edit', 'Редактирование роли'],
  ['users/login', 'Вход в систему'],
]);

// CREATE TABLE public.templates (
// 	id serial4 NOT NULL,
// 	"name" varchar NOT NULL,
// 	employee_count int4 NOT NULL,
// 	file_path varchar NOT NULL,
// 	message_id int4 NOT NULL,
// 	doc_start_date date NOT NULL,
// 	gen_start_date date NULL,
// 	npa_date date NULL,
// 	is_deleted bool NOT NULL DEFAULT false,
// 	instruction_id int4 NOT NULL DEFAULT 1,
// 	npa_name varchar NULL
// );
