export interface RoleType {
    id?: number;
    title: string;
    permissions?: string[];
}