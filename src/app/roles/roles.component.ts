import { Component, OnInit, Inject, Optional, AfterViewInit, ViewChild} from '@angular/core';
import { DocbackService } from '../services/docback.service';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { RoleType } from './role_type';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AddRoleComponent } from './add/add.component';
import { MatPaginator } from '@angular/material/paginator';
import {TranslateService} from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
})

export class RolesComponent implements OnInit, AfterViewInit{
  @ViewChild(MatTable, { static: true }) table: MatTable<any> = Object.create(null);
  searchText: any;
  dataSource: MatTableDataSource<RoleType> = new MatTableDataSource<RoleType>([]);
  displayedColumns: string[] = ['#', 'title', 'permissions', 'actions'];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator = Object.create(null);

  constructor(
    public dialog: MatDialog,
    public docBack: DocbackService,
    translate: TranslateService) {
    translate.setDefaultLang('en');
    translate.use('ru');
}

  ngOnInit(): void {
    this.docBack.getList<RoleType>(0, 10000, 'roles').subscribe(data => {
      this.dataSource.data = data;
      console.log(data);
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialog(action: string, obj: any): void {
    obj.action = action;
    const dialogRef = this.dialog.open(RoleTypeDialogContent, {
        data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.event === 'Add') {
          this.addRowData();
        } else if (result.event === 'Update') {
          this.updateRowData();
        } else if (result.event === 'Delete') {
          this.deleteRowData();
        } else if (result.event === 'Error') {
          this.dialog.open(AddRoleComponent);
        }
      }
    });
  }

  addRowData(): void {
    window.location.reload();
  }

  updateRowData(): boolean | any {
    window.location.reload();
  }

  deleteRowData(): boolean | any {
    window.location.reload();
  }

}

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'dialog-content',
  templateUrl: 'dialog-content.html',
  styleUrls: ['./dialog.component.scss']
})
// tslint:disable-next-line: component-class-suffix
export class RoleTypeDialogContent {
  action: string;
  // tslint:disable-next-line - Disables all
  local_data: any;
  local_data_copy: any;
  file: any;
  joiningDate: any = '';
  formData = new FormData();
  constructor(
      private http: HttpClient,
      public docBack: DocbackService,
      public datePipe: DatePipe,
      public dialogRef: MatDialogRef<RoleTypeDialogContent>,
      // @Optional() is used to prevent error if no data is passed
      @Optional() @Inject(MAT_DIALOG_DATA) public data: RoleType
  ) {
      this.local_data = { ...data };
      this.local_data_copy = JSON.parse(JSON.stringify(this.local_data));
      this.action = this.local_data.action;
      if (this.local_data.DateOfJoining !== undefined) {
          this.joiningDate = this.datePipe.transform(new Date(this.local_data.DateOfJoining), 'yyyy-MM-dd');
      }
  }
  log1(a: string): void {
    console.log(a);
  }

  doAction(): void {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-User': this.docBack.currentUser ? this.docBack.currentUser.email : '',
      })
    };
    if (this.local_data.action === 'Delete') {
      const href = `${this.docBack.baseUrl}roles/edit`;
      this.local_data.is_deleted = true;
      this.local_data.copy = this.local_data_copy;
      this.http.post(href, this.local_data, httpOptions).subscribe(
        template => {
          this.dialogRef.close({event: this.action, data: template});
        },
        error => {
          this.dialogRef.close({event: 'Error', data: error});
        }
      );
    }
    if (this.local_data.action === 'Add') {
      const href = `${this.docBack.baseUrl}roles/add`;
      this.http.post(href, this.local_data, httpOptions).subscribe(
        template => {
          this.dialogRef.close({event: this.action, data: template});
        },
        error => {
          this.dialogRef.close({event: 'Error', data: error});
        }
      );
    }
    if (this.local_data.action === 'Update') {
      const href = `${this.docBack.baseUrl}roles/edit`;
      this.local_data.copy = this.local_data_copy;
      this.http.post(href, this.local_data, httpOptions).subscribe(
        template => {
          this.dialogRef.close({event: this.action, data: template});
        },
        error => {
          this.dialogRef.close({event: 'Error', data: error});
        }
      );
    }
  }
  closeDialog(): void {
      this.dialogRef.close({ event: 'Cancel' });
  }
  clickEd(event: any): void {
    console.log(event);
    event.target.firstChild.click();
  }
}
